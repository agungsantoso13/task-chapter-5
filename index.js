const http = require('http');
const fs = require('fs');

const onRequest = (request, response) => {

    console.log(request.url, request.method);

    let path = './views/';

    switch (request.url) {
        case '/home':
            path = path + 'home.html';
            response.statusCode = 200;
            break;
        case '/about-us':
            path = path + 'about-us.html';
            response.statusCode = 200;
            break;
        case '/contact-us':
            path = path + 'contact-us.html';
            response.statusCode = 200;
            break;
        case '/':
            response.statusCode = 301;
            response.setHeader('location', '/home');
            response.end();
            break;

        default:
            path = path + '/404.html';
            response.statusCode = 404;
            break;
    }

    response.setHeader("Content-Type", "text/html");

    fs.readFile(path, null, (err, data) => {
        if (err) {
            response.writeHead(404);
            response.write("File Not Found");
        } else {
            response.write(data);
        }
        response.end();
    })

};


http.createServer(onRequest).listen(8000, 'localhost', () => {
    console.log(`Listening for request on http://localhost:8000`);
});